from typing import *

import numpy as np
import pytest

from boss.bo.bo_main import BOMain
from boss.io.parse import parse_acqs
from boss.pp.pp_main import PPMain
from boss.utils.testing import TestFunc, execute_in, get_test_func


class FakeSymmetryFunc:
    def __init__(self, test_func: TestFunc, batch_max: int = 3) -> None:
        self.parent = test_func
        self.batch_max = batch_max

    @property
    def bounds(self):
        return self.parent.bounds
    
    @property
    def frange(self):
        return self.parent.frange

    def __call__(self, X: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
        f = self.parent
        n = np.random.randint(0, self.batch_max)
        X_new = (
            f.bounds[:, 0].T
            + np.random.random_sample((n, f.ndim)) * (f.bounds[:, 1] - f.bounds[:, 0]).T
        )
        X_new = np.vstack((X, X_new))
        Y = f(X_new)
        return X_new, Y


@pytest.mark.integration
def test_userfn_symmetry(tmp_path):

    with execute_in(tmp_path):
        test_func = get_test_func("Himmelvalley")
        f = FakeSymmetryFunc(test_func, batch_max=3)

        initpts = 5
        iterpts = 5
        bo = BOMain(
            f,
            bounds=f.bounds,
            yrange=f.frange,
            initpts=initpts,
            iterpts=iterpts,
            optimtype="score",
            seed=2165,
        )
        res = bo.run()

        _, ind_lims = parse_acqs(bo.settings, "boss.out")
        bs0 = ind_lims[initpts]
        batch_sizes = np.insert(np.diff(ind_lims[initpts:]), 0, bs0)

        for i in range(iterpts):
            assert len(res.select("X", i)) == batch_sizes[i]

        # also test post-processing
        pp = PPMain(res, pp_models=True, pp_acq_funcs=True)
        pp.run()
