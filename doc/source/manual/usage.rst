BOSS inputs and outputs
============================

To run BOSS, it is necessary to set a few essential keywords.
(see also the :ref:`tutorials <Tutorials>` section.)
The BOSS binary for HPC platforms requires an ``ascii`` **input file** 
(any name) and a customised **user function** Python script.
An example input file is shown below:

.. code-block:: python

    userfn obj_alanine.py  # name of custom user fn
    bounds -50 310; -50 310; -50 310  # the boundaries of the search space
    yrange 0 100  # estimated variation of the f(x) data
    initpts 5  # initial points
    iterpts 100  # BO points

    # Postprocessing options
    pp_models true
    pp_model_slice 1 2 50

    # RESULTS
    # data in form of x_i, f(x_i)


BOSS will perform a search across the phase space defined by the 
boundaries in the :math:`x_i` bounds. The number of boundary pairs 
defines the search dimensionality (3D above). The ``yrange``
variable is employed to initialise priors on the GP 
hyperparameters. The RESULTS
keyword provides a way to read in previously obtained data and 
fit surrogate models, but is not essential to new BO runs. 
This mechanism allows continuation and restart runs.

BOSS simulations are directed by a large number of customisable keywords, 
allowing the user to vary advanced options such as data noise,
hyperparameter choices and priors, acquisition functions, etc. 
BOSS defaults have been carefully selected for stable and robust 
BO in a wide range of examples. Unless explicitly specified in the 
input file, keywords will be assigned default values. The full
list of keywords and their default values can be found :ref:`here<Keywords>`.

.. raw:: html

   <h3>BOSS output</h3>


BOSS binary writes two output files: **boss.out** and **boss.rst** (custom names
allowed in input file). Note, repeated BOSS runs will overwrite these files 
unless they are renamed. Equivalent information can be retrieved from the BOSS API.

The summary of the simulation, iterative procedure, convergence,
hyperparameters and key findings can be found in **boss.out**. 
Simulation restarts are facilitated by **boss.rst**, which contains the history 
of acquisitions and fitted model parameters. Both files are humanly
readable, and also serve as input for BO postprocessing routines.
By :ref:`postprocessing<BO postprocessing>` a BO run, it is possible to visualise 
data acquisition trends, simulation convergence and obtain snapshots of the
surrogate models. 

.. In multidimensional runs, models can only be
  visualised in 2D slices (with other variables at :math:`\hat{x}`
  values). Postprocessing also allows grid-based computation of the true
  function :math:`f(x)` (used to validate BO results in low dimensions)
  and true function evaluation at :math:`\hat{x}` (used to evaluate the
  convergence and accuracy of the surrogate model). All graphs and raw
  data files can be found in the ``postprocessing`` folder.

.. raw:: html

   <h3>BOSS run cases</h3>

BOSS is intended for use on HPC platforms to support parallelised data
acquisitions on CPUs. For best performance, please set the
environment variable ``OMP_NUM_THREADS=1``. Parts of BOSS related to 
minimization routines are parallelized and provide significant speed-ups,
this functionality can be switched on by :ref:`keywords<Keywords>`.

At present, BO and postprocessing (PP) simulations can be carried out
separately. This is to allow the bulk of the BO simulations to be
carried out on a supercomputer; only the two output files need to be
transferred to a local machine, where the postprocessing can be
carried out, and the results easily visualised. In this sense, BOSS
supports several different run cases:

#. Continuous BO and PP: **boss op myinput**. Input file must feature pp
   keywords.

#. Pure BO: **boss o myinput** . Input file may feature pp keywords, but
   they will not be read and postprocessing will not be performed.

#. Pure PP: **boss p modified.rst boss.out**. The boss.rst from a previous
   BO run should be modified to add pp keywords. Both \*.rst and \*.out file are
   needed.

#. **Restart** from a previous BOSS run: the latest \*.rst file is employed
   as input. ``initpts`` should be augmented to the total number of points
   acquired to date, and ``iterpts`` should be set to the total desired 
   dataset size.

#. **Start with previous data:** any approprately formatted data can be inserted 
   under the keyword RESULTS to construct a GP model (``initpts`` should be set to
   number of data lines).


